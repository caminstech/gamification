class { '::mysql::server':
    root_password  => 'lti',
    override_options => {
        mysqld => {
            'bind_address' => '0.0.0.0',
        }
    }
}
mysql::db { 'lti':
    user     => 'lti',
    password => 'lti',
    host     => '%',
    charset  => 'utf8',
}

class { 'apache':
    default_vhost => false,
    mpm_module    => 'prefork',
    purge_configs => true,
}

include 'apache::mod::php', 'apache::mod::rewrite'

apache::vhost { 'lti':
    servername => 'lti',
    port    => '80',
    ssl     => false,
    docroot => '/vagrant/apps/lti',
}

include 'php'
Php::Extension <| |> -> Php::Config <| |> ~> Service['apache2']
class { ['php::cli', 'php::extension::mysql']:
}
