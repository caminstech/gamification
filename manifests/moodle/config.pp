class { '::mysql::server':
    root_password  => 'moodle',
    override_options => {
        mysqld => {
            'bind_address' => '0.0.0.0',
        }
    }
}
mysql::db { 'moodle':
    user     => 'moodle',
    password => 'moodle',
    host     => '%',
    charset  => 'utf8',
}

class { 'apache':
    default_vhost => false,
    mpm_module    => 'prefork',
    purge_configs => true,
}

include 'apache::mod::php', 'apache::mod::rewrite'

apache::vhost { 'moodle':
    servername => 'moodle',
    port    => '80',
    ssl     => false,
    docroot => '/vagrant/apps/moodle',
}

include 'php'
Php::Extension <| |> -> Php::Config <| |> ~> Service['apache2']
class { ['php::cli', 'php::extension::mysql', 'php::extension::curl', 'php::extension::gd', 'php::extension::intl']:
}
