apt-get update -y
apt-get upgrade -y

apt-get install -y puppet
puppet module install --force puppetlabs-apache
puppet module install --force puppetlabs-mysql
puppet module install --force nodes/php
