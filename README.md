# Documentació sobre LTI

LTI(Learning Tools Interoperability) és un protocol de connexió entre una plataforma educativa i els mòduls que es vulguin afegir.
Aquest protocol permet que seguint les seves pautes es pugui crear un mòdul únic per totes les plataformes educatives que
permeten la seva integració (rarament a dia d'avui alguna plataforma no l'implementarà). El protocol està implementat per la companyia IMS GLOBAL, aquest és l'enllaç on està explicat el protocol i com implementar-lo segons la companyia:

 - https://www.imsglobal.org/activity/learning-tools-interoperability
 - https://www.imsglobal.org/sites/default/files/lti/v1p1p1/images/ltiIMGv1p1p1-image002.gif

Aquest protocol usa el traspas d'inforació entre la plataforma educativa i el mòdul externs agilitzant no haver de tornar a fer login en l'aplicació externa, i a la vegada retorna la informació a la plataforma educativa (ja sigui un resultat d'un test, o la participació d'un alumne, etc).

 - https://www.imsglobal.org/sites/default/files/lti/image05.png

D'aquest protocol hi ha dues versions on LTI v2 és una versió extesa de LTI v1. Està dissenyat per estendre la interoperabilitat a tot el cicle de vida de l'eina no només per al punt de llançament. Aquest canvi és tan profund que permet LTI 2.0 per ser una plataforma no només per la incorporació d'una eina, sinó també per a la creació de d'eines d'adaptació complexes.
 - Explicació més extesa sobre LTI : https://net.educause.edu/ir/library/pdf/ELI7099.pdf
 - Exemple Youtube LTI App http://brianwhitmer.blogspot.com.es/2012/12/building-youtube-lti-app.html

## Llibreries i exemples LTI

- https://www.imsglobal.org/learning-tools-interoperability-sample-code

Aquest link és un conjunt de repositoris en diferents codis de programació en els quals hi ha diferents exemples de codi
d'apps amb implementació LTI. Cada implementació està feta per diferents companyies les quals dins de les seves pàgines
també tenen explicat com ho han implementat el protocol.

En els exemples molts cops implementaven tota la connectivitat usant el protocol mitjançant XML, sempre i quan no l'implementan a la vegada que fan el servidor. Aquí hi ha un recull de les funcions usades:

- oauth_consumer_key: Required. Matches the configuration’s key.
- oauth_nonce: Required. An arbitrary number only used once.
- oath_timestamp: Required. A unix timestamp for when the signature was signed.
- tool_consumer_instance_guid: Required. Unique identifier of your consumer install.
- tool_consumer_info_product_family_code: Required. Type of consumer (eg: Canvas. Obojobo). Used to determine  configuration settings.
- resource_link_id: Required. Unique id for this widget use.
- roles: Required. This role tells Materia to expect to show the selection screen.
- launch_presentation_return_url: Used by the picker interface to set the link to this widget in the consumer. [Optional]
- context_id: ID of the course. [Optional]
- context_title: Name of the course. [Optional]
- user_id: User ID of the user performing the operation. [Optional]
- lis_outcome_service_url: Optional. If set, Materia sends the final widget score to this url. See Score Passback above. [Optional]
- lis_result_sourcedid: ID sometimes required by the consumer to return scores. [Optional]
- lis_person_sourcedid: Often used as the user ID to match in Materia’s local data. [Optional]
- lis_person_name_family: User’s last name. [Optional]
- lis_person_name_given: User’s first name. [Optional]
- lis_person_contact_email_primary: Useful if Materia is set up to create users on LTI handshakes. [Optional]
- custom_widget_instance_id: Some systems (like Obojobo) know the ID of the widget it’s requesting. [Optional]

OAuth és el protocol emprat per no haver de tornar a fer login a l'app externa. Com a mòdul extern ens em d'encarregar
d'oferir un token i una key a la plataforma educativa ja que d'aquesta manera indentifiqui l'accés a la nostre app.

## Javascript
- Especificació OAuth: https://oauth.net/core/1.0/
- Repositori express (Javscript): https://github.com/expressjs/express
- Documentacio tecnica implementacio LTI en Node.js http://www.dev.nsip.edu.au/sites/nsip.edu.au/files/SIF%20-%20IMS%20LTI%20Consumer%20v1%200%20Technical.pdf

## Repositori

En el repositori que tenim hi ha:

 - Instància de Moodle i app buida (per entrar a moodle user:admin password: Admin1234)
 - Al directori **proves** hi ha una prova bàsica d'un servidor amb node.js.
 - Al directori **HITS-LTIJS**hi ha el repositori de l'exemple d'app implementada en JS amb protocol LTI i una carpeta myapp on està en
   procès fer tot allò de proves però amb express.
 - Al directori **binari** hi ha les proves per la gamificació de la conversió a binari.
